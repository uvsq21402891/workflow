
package fr.uvsq.uvsq21501748.workflow;

public final class Fraction implements Comparable<Fraction> {
	
	public static final Fraction ZERO = new Fraction(0, 1);
	public static final Fraction UN = new Fraction(1, 1);

	private int num;
	private int den;

	public Fraction(int num, int den) {
		if (den == 0)
			throw new ArithmeticException();
		this.num = num;
		this.den = den;
	}

	public Fraction(int num) {
		this(num, 1);
	}

	public Fraction() {
		this(0, 1);
	}

	public Object getNum() {
		return num;
	}

	public Object getDen() {
		return den;
	}

	public double flottant() {
		return num / (den * 1.0);

	}

	@Override
	public String toString() {
		return num + "/" + den;
	}

	public int compareTo(Fraction frac) {
		return 10 * (int)(this.flottant() - frac.flottant());
	}

}
