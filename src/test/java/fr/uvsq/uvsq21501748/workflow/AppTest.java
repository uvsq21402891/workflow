package fr.uvsq.uvsq21501748.workflow;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

public class AppTest {

	@Test
	public void jonatConstanteTest() {
		Fraction f = Fraction.UN;
		assertEquals(1, f.getNum());
	}

	@Test
	public void jonat2Test() {
		Fraction f = new Fraction(2);
		assertEquals(2, f.getNum());
		assertEquals(1, f.getDen());

	}

	@Test
	public void jonatFlottantTest() {
		Fraction f = new Fraction(1, 2);
		assertEquals(new Double(0.5), new Double(f.flottant()));

	}

	@Test(expected = ArithmeticException.class)
	public void testException() {
		Fraction f = new Fraction(1, 0);

	}

	@Test
	public void maximeTestFraction() {
		Fraction frac = new Fraction();
		assertEquals(0, frac.getNum());
		assertEquals(1, frac.getDen());
	}

	@Test
	public void maximeTestConstante() {
		Fraction frac = Fraction.ZERO;
		assertEquals(0, frac.getNum());
		assertEquals(1, frac.getDen());
	}

	@Test
	public void maximeTestString() {
		Fraction frac = new Fraction(1, 2);
		assertEquals("1/2", frac.toString());
	}
	
	@Test
	public void testCompareFraction() {
		Fraction frac1 = new Fraction(1, 2);
		Fraction frac2 = new Fraction(3, 4);
		Fraction frac3 = new Fraction(5, 6);
		List<Fraction> al = new ArrayList<Fraction>();
		Collections.addAll(al, frac1, frac2, frac3); 
		assertEquals(frac3, Collections.max(al));
		assertEquals(frac1, Collections.min(al));
	}

}
